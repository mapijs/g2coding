<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Workshop for dummies</title>

    <!-- javascript imports -->
    <script src="/src/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="retrieveAndEval.js" type="text/javascript"></script>

    <!-- css resources -->
    <link rel="stylesheet" href="css/styling.css">
    <link rel="stylesheet" href="css/ACE.css">
    <link rel="stylesheet" type="text/css" href="css/Header-Nightsky.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

</head>
<body>
<div class="header-nightsky">
    <div id="header">
        <jsp:include page="includes/header.jsp"/>
    </div>

<input type="hidden" id="testNumber" value="2">

<div id="editBox">
    <h3><br/>input</h3>
    <div id="editor">

//Restriction enzymes are useful enzymes that cut a DNA sequence in two pieces.
//Every restriction enzyme has their own restriction site (a specific combination of nucleotides) where they cut.
//The restriction site of the enzyme EcoRI is:
var EcoRI = "GAATTC";
//The cut would result the sequence "G AATTC"
//Find all occurrences of the restriction site of EcoRI in the following DNA sequence
//and replace them with the cut sequence.
var DNA = "CGATTTGCGATGCAGGAATTCGCCGAATTGTGAATTCGCGTGAATTCGGAATTC";
    </div>
</div>
<div id="outputBox">
    <h3><br/>output</h3>
    <div id="output">
//tip!: you can use regex to find the sequence and str.replace(*pattern*, *replacement*)
//to replace the pattern with the replacement.
//Regex uses a pattern to search through a String.
        // Patterns are defined with '/' for example /hello/ would find the first instance of 'hello'
        // you can modify your pattern with modifiers:
            // i makes your pattern match case-insensitive
            //(example: /hello/i matches both 'hello' and 'Hello')
            // g makes your pattern match globally
            //(example: /hello/g matches all instances of 'hello' in a String not just the first)
            // m makes your pattern match on multiple lines
--------------------------------------------
    </div>
</div>
<button id="submit" class="btn btn-success btn-lg">submit</button>
<form action="home.jsp">
    <input id="nextButton" class="btn btn-success btn-lg" type="submit" value="Finished"/>
</form>
</div>

<div id="footer">
    <jsp:include page="includes/footer.jsp"/>
</div>
</body>
</html>
