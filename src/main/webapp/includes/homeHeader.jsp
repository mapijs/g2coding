<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-default">
    <div class="container">
        <a class="navbar-brand">G2Coding</a>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a>Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">Tools <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="jquerySortable.jsp">Jquery sorter</a></li>
                        <li><a href="textEditor1.jsp">Code editor</a></li>
                        <li><a href="sandbox.jsp">Sandbox mode</a></li>
                    </ul>
                </li>
                <li><a href="contact.jsp">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>