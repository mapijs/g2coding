<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-default">
    <div class="container">
        <a class="navbar-brand">G2Coding</a>
        <div class="navbar-header">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="home.jsp">Home</a></li>
            </ul>
        </div>
    </div>
</nav>