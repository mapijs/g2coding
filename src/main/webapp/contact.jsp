<%--
  Created by IntelliJ IDEA.
  User: mrmulder
  Date: 8-1-18
  Time: 9:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Workshop for dummies</title>

    <!-- css resources -->
    <link rel="stylesheet" href="css/styling.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/Header-Nightsky.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>
<body>
<div class="header-nightsky">
    <h1>
        Contact information
    </h1>
    <p>Hanze Hogeschool, Groningen</p>
    <p>Matthijs Mulder (co-author): m.r.mulder@st.hanze.nl</p>
    <p>Roland Hut (co-author): r.g.hut@st.hanze.nl</p>


</div>
<div id="footer">
    <jsp:include page="includes/footer.jsp"/>
</div>
</body>
</html>
