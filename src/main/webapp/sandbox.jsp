<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Workshop for dummies</title>

    <!-- javascript imports -->
    <script src="/src/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="retrieveAndEval.js" type="text/javascript"></script>

    <!-- css resources -->
    <link rel="stylesheet" href="css/ACE.css">
    <link rel="stylesheet" href="css/styling.css">
    <link rel="stylesheet" type="text/css" href="css/Header-Nightsky.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">



</head>
<body>
<div class="header-nightsky">
    <div id="header">
        <jsp:include page="includes/header.jsp"/>
    </div>

<input type="hidden" id="testNumber" value="sandbox">

<div id="editBox">
    <h3><br/>input</h3>
    <div id="editor">//Sandbox mode. Code all you want.

    </div>
</div>
<div id="outputBox">
    <h3><br/>output</h3>
    <div id="output"></div>
</div>
<button id="submit" class="btn btn-success btn-lg">submit</button>
</div>
</div>

<div id="footer">
    <jsp:include page="includes/footer.jsp"/>
</div>
</body>
</html>





