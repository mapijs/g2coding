/**
 * Created by mrmulder on 22-12-17.
 */


//Loads highlighting.
$(document).ready(function () {
    $('ul').css('list-style', 'none');
    $('pre code').each(function (i, e) {
        hljs.highlightBlock(e)
    });
});


//When the order of the list is changed, gives new order of positions.
$(function () {
    $(".ui-state-default").sortable({
        update: function (event, ui) {
            //records new order of elements when user changed the order.
            var changedList = this.id;
            var order = $(this).sortable("toArray");
            var positions = order.join(";");

            //Checks on which page the user currently is, and which answersheet should be used.
            switch (pageTitle = $("body").data("title")) {
                case "calculator":
                    var AnswerPositions = "2;1;4;3";
                    break;
                case "food":
                    var AnswerPositions = "3;2;4;1";
                    break;
                case "cars":
                    var AnswerPositions = "3;2;1;4";
                    break;
            }

            if (positions == AnswerPositions) {
                //Makes the "next" button visable when the user solved the problem.
                document.getElementById("nextButton").style.visibility = 'visible';
            }
            else {
                document.getElementById("nextButton").style.visibility = 'hidden';
            }
        }
    });

});
