<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Workshop for dummies</title>

    <!-- javascript imports -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
    <script src="JquerySortable.js"></script>

    <!-- css resources -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/styling.css">
    <link rel="stylesheet" type="text/css" href="css/Header-Nightsky.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

</head>
<body data-title="food">
<div class="header-nightsky">
    <div id="header">
        <jsp:include page="includes/header.jsp"/>
    </div>

    <ul id="sortable" class="ui-state-default">
        <li id="1"><pre><code class="javascript">

else{
console.log("i only know two types of fruit alright!");
}
            </code></pre>
        </li>
        <li id="2"><pre><code class="javascript">
if (food == "apple") {
console.log("i like apples");
}
            </code></pre>
        </li>
        <li id="3"><pre><code class="javascript">
var food = "kiwi";
            </code></pre>
        </li>
        <li id="4"><pre><code class="javascript">
else if (food == "pineapple") {
console.log("i don't like pineapple!");
}
            </code></pre>
        </li>
    </ul>
    <form action="jquerySortable2.jsp">
        <input id="nextButton" class="btn btn-success btn-lg" type="submit" value="Next"/>
    </form>
</div>

<div id="footer">
    <jsp:include page="includes/footer.jsp"/>
</div>
</body>
</html>





