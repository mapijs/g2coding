<%--
  Created by IntelliJ IDEA.
  User: mrmulder
  Date: 8-1-18
  Time: 9:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Workshop for dummies</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- javascript imports -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- css resources -->
    <link rel="stylesheet" href="css/styling.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/Header-Nightsky.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

</head>
<body>
<div class="header-nightsky">
    <div id="header">
        <jsp:include page="includes/homeHeader.jsp"/>
    </div>

    <div id="text">
        <h1>
            Workshop for dummies
        </h1>
        <h2>
            What is this site?
        </h2>
        <p>
            This site is known as g2coding, it is made with the intention of teaching prospective students some basics
            of coding.
            <br/>
            The language used in the exercises is known as javascript. It is not a language normally learned early on in
            bio-informatics, but it is used because of its simplicity.
            <br/>
            Most of the statements used in javascript are just as easily used in other languages, sometimes only the way
            it is written changes.
        </p>
        <h2>
            What will you be able to do after this workshop?
        </h2>
        <p>
            We hope to teach you basic understanding of programming. Basic statements like for loops, if/else and a few
            other small but very important things to know.
        </p>

        <h2>
            This site is still under construction
        </h2>
        <p>
            We are aware that the difficulty scaling of the exercises is not yet optimised.
            <br/>
            In case this program becomes an acknowledged application for students to use, we will make sure to keep
            developing it.
        </p>
    </div>
</div>

<div id="footer">
    <jsp:include page="includes/footer.jsp"/>
</div>
</body>
</html>
