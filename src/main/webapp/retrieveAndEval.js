/**
 * Name: retrieveAndEval
 * Version: 1.0.180118
 * Author: RGHut
 * Use: handles and manipulates Ace editor and output.
 */

window.onload = function() {
    // list of answers to the exercises
    var answerList = ["ATCGTATGCGCTTACGGACTGCAATTGCCGTACGTAGCAGCTTAGCAGTCAGTTACCGTA",
        "A = 25.53%, T = 23.4%, C = 25.53%, G = 25.53%",
        "CGATTTGCGATGCAGG AATTCGCCGAATTGTG AATTCGCGTG AATTCGG AATTC"];
    var sandbox = false;
    // behaves differently if sandbox mode is enabled
    if (document.getElementById("testNumber").value.toString() == "sandbox") {
        sandbox = true;
    }
    else {
        var testNumber = document.getElementById("testNumber").value.toString();
    }

    // creates the editor and sets theme/mode
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/idle_fingers");
    editor.getSession().setMode("ace/mode/javascript");

    // creates the output box and sets theme/mode
    var output = ace.edit("output");
    output.setTheme("ace/theme/idle_fingers");
    output.getSession().setMode("ace/mode/java");

    document.getElementById("submit").onclick = function(){
        // retrieves the code from editor, runs it, and prints it in output
        var result = "code has no output"; //set default result value
        try { //try to eval the code, if code has output set result value to that output
            var evaled = eval(editor.getValue());
            if (evaled !== undefined) {
                result = evaled;
            }
        } catch (e) { //if code has errors set result value to error message and insert pointer in line with error
            result = "code has errors on line: " + e.lineNumber + ", " + e.message;
            editor.gotoLine(e.lineNumber);
            editor.navigateLineEnd();
            editor.insert("  //<--ErrorInCode:  " + e.message);
        }

        // inserts result in output
        output.navigateFileEnd();
        output.insert(result.toString());
        output.insert("\n--------------------------------------------\n");
        if (!(sandbox)) {
            if (comparer(result.toString(), testNumber)) {
                // if user created code has the expected return value, congratulates them and allows them to proceed to the next exercise.
                output.navigateFileEnd();
                output.insert("\nWell done");
                unhideNextButton();
            }
            else {
                output.navigateFileEnd();
                output.insert("\nNeeds some more work. Try again.");
            }
        }
    };

    function unhideNextButton() {
        // makes the nextButton available
        document.getElementById("nextButton").style.visibility = 'visible';

    }

    function comparer(userOutput, n) {
        // compares users output with expected value
        if (userOutput != answerList[n]) {
            console.log("FALSE");
            return false;
        }
        else {
            console.log("TRUE");
            return true;
        }

    }


};
