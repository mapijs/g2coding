#Gates to Coding

Gates to coding, abbreviated G2Coding is meant to be a stepping stone into programming.
Providing an easy way to start learning programming, and giving clear examples why programming is necessary in bio-informatics.

##Getting started

To use this project, fork this repository to your own bitbucket and local machine. 

##Prerequisites

up-to-date HTML and JavaScript.

##Installing

Installation is not required. 

##Authors

* Matthijs Mulder - developer
* Roland Hut - developer

##License

Copyright 2018 Matthijs Mulder and Roland Hut.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

##Acknowledgments

We wish to thank Marcel Kempenaar for his supervision and support.
